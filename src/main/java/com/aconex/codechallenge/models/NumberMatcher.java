package com.aconex.codechallenge.models;

public class NumberMatcher {
  private String numbers;
  private String word;

  public String getNumbers() {
    return numbers;
  }

  public void setNumbers(String numbers) {
    this.numbers = numbers;
  }

  public String getWord() {
    return word;
  }

  public void setWord(String word) {
    this.word = word;
  }
}
