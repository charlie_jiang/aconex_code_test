1800 code challenge
======
This is a simple command line app to convert 1800 number to easy remember numbers

##Prerequirement
1. JAVA 8
2. Gradle 3.5
3. Maven

##Build

Windows:

`gradlew.bat build`

Linux/Mac:

`./gradlew build`

If you are using Winodws, please always run `gradlew.bat` instead of `./gradlew` below.
## Run
1. Simply run `./gradlew run`
4. By default it will pickup csv here `src/main/resource/input.csv` and write output CSV into the folder where you run this app.

## Test
1. Simply run `./gradlew test`

##Configuration
1. You can specify your input csv and out put csv by using `./gradlew run -DinputCSVFile=/youroutputcsvfilepath -DoutputCSVFile=/youroutputcsvfilepath`
